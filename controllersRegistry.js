const controllers = {
    search: {
        path: './controllers/search'
    },
    home: {
        path: './controllers/home',
        prefix: '/'
    },
    product: {
        path: './controllers/product'
    },
    cart: {
        path: './controllers/cart'
    }
};

module.exports = controllers;
