const basketMgr = require('./basketMgr');

function updateMiniCartQuantity() {
    const totalQuantity = basketMgr.getTotalQuantity();
    $('.js-minicart-quantity').text(totalQuantity || '');
}

function updateProduct(product) {
    const span = product.find('.js-quantity');
    if (span.text() > 1) {
        span.text(span.text() - 1);
    } else {
        product.remove();
    }
}

function initEvents() {
    $(document).on('click', '.js-add-to-cart', function (event) {
        event.preventDefault();
        const pid = $(this).closest('.js-product').data('pid');
        if (pid) {
            basketMgr.addProduct(pid);
            updateMiniCartQuantity();
        }
    });
    $(document).on('click', '.js-remove-from-cart', function (event) {
        event.preventDefault();
        const product = $(this).closest('.js-product');
        const pid = product.data('pid');
        if (pid) {
            basketMgr.deleteProduct(pid);
            updateMiniCartQuantity();
            updateProduct(product);
        }
    });

    $(document).on('click', '.js-minicart-icon', function (event) {
        event.preventDefault();
        const url = $(this).attr('href');

        $.ajax({
            url: `${window.location.origin}/${url}`,
            method: 'GET',
            data: {
                basket: basketMgr.getOrCreateBasket()
            },
            success: (data) => {
                $('.js-minicart').html(data).show();
            }
        });
    });

    $(document).on('click', '.js-cart-page', function (event) {
        event.preventDefault();
        const url = $(this).attr('href');
        const params = { basket: basketMgr.getOrCreateBasket() };
        const paramStr = $.param(params);
        window.location.href = `${window.location.origin}/${url}?${paramStr}`;
    });

    $(document).on('click', '.js-close-minicart', (event) => {
        event.preventDefault();
        $('.js-minicart').empty();
    });
}

function init() {
    initEvents();
    updateMiniCartQuantity();
}

module.exports = {
    init
};
