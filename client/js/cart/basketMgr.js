/* eslint-disable no-param-reassign */
const json = require('../utils/json');

function getOrCreateBasket() {
    const basket = localStorage.getItem('basket');
    if (basket) {
        return json.parse(basket, {});
    }
    return {};
}
function putProductToBasket(pid, basket) {
    const productKey = `pid_${pid}`;
    const alreadyInBasket = basket[productKey];
    if (alreadyInBasket) {
        const { quantity } = alreadyInBasket;
        if (typeof quantity === 'number') {
            alreadyInBasket.quantity = quantity + 1;
        } else {
            alreadyInBasket.quantity = 1;
        }
        basket[productKey] = alreadyInBasket;
    } else {
        basket[productKey] = {
            id: pid,
            quantity: 1
        };
    }
}
function deleteProductToBasket(pid, basket) {
    const productKey = `pid_${pid}`;
    if (basket[productKey]) {
        const { quantity } = basket[productKey];
        if (typeof quantity === 'number') {
            basket[productKey].quantity = quantity - 1;
            if (basket[productKey].quantity <= 0) {
                delete basket[productKey];
            }
        } else {
            delete basket[productKey];
        }
    }
}

function saveBasket(basket) {
    localStorage.setItem('basket', JSON.stringify(basket));
}
function addProduct(pid) {
    const basket = getOrCreateBasket();
    putProductToBasket(pid, basket);
    saveBasket(basket);
}
function deleteProduct(pid) {
    const basket = getOrCreateBasket();
    deleteProductToBasket(pid, basket);
    saveBasket(basket);
    if (Object.keys(basket).length === 0) {
        document.getElementById('h1').innerHTML = ('The cart page is blank');
    }
}
function getTotalQuantity() {
    const basket = getOrCreateBasket();
    let quantity = 0;
    Object.keys(basket).forEach((key) => {
        const item = basket[key];
        if (item && item.quantity) {
            quantity += item.quantity;
        }
    });
    return quantity;
}
module.exports = {
    deleteProduct,
    addProduct,
    getTotalQuantity,
    getOrCreateBasket
};
